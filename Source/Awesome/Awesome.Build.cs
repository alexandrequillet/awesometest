// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Awesome : ModuleRules
{
	public Awesome(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput", "GameplayCameras", "Niagara" });
	}
}
