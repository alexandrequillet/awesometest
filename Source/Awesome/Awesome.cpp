// Copyright Epic Games, Inc. All Rights Reserved.

#include "Awesome.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Awesome, "Awesome" );
 