// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "Curves/CurveFloat.h"
#include "LegacyCameraShake.h"
#include "AwesomeCharacter.generated.h"

// Forward declarations
class USplineComponent;
class ULegacyCameraShake;

UCLASS(config=Game)
class AAwesomeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
	/** Niagara particule */
	class UNiagaraComponent* Niagara;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookAction;

public:
	AAwesomeCharacter();
	
protected:

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	void StartSliding();
	void StopSliding();

	UFUNCTION()
	void OnComponentHit(UPrimitiveComponent* OnComponentHit, UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	virtual void Jump() override;

	virtual void Tick(float DeltaTime) override;

private:
	/** Sliding**/
	UPROPERTY(EditAnywhere, Category = "Sliding")
	FRuntimeFloatCurve SlindingCurve;
	UPROPERTY(EditAnywhere, Category = "Sliding")
	FRuntimeFloatCurve CameraFOVCurve;
	UPROPERTY(EditAnywhere, Category = "Sliding")
	FRuntimeFloatCurve CameraLagCurve;
	UPROPERTY(EditAnywhere, Category = "Sliding")
	FRuntimeFloatCurve ParticuleCurve;

	UPROPERTY(EditAnywhere, Category = "Sliding")
	TSoftClassPtr<ULegacyCameraShake> CameraShake;

	float StartSplineDistance = 0.f;
	float CurrentSplineDistance = 0.f;
	double SlideDirection = 0.f;
	float CurrentSlindingSpeed = 0.f;
	float CurrentSlidingDistance = 0.f;
	bool bAlreadyPlayedShake = false;
	USplineComponent* CachedSplineComponent = nullptr;
	FRotator OldCharacterRotation;


};
