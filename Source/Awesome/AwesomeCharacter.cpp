// Copyright Epic Games, Inc. All Rights Reserved.

#include "AwesomeCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Components/SplineComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "NiagaraComponent.h"

//////////////////////////////////////////////////////////////////////////
// AAwesomeCharacter

AAwesomeCharacter::AAwesomeCharacter()
{
	// Let's tick for sliding system
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}

void AAwesomeCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	UCapsuleComponent* MyCapsuleComponent = GetComponentByClass<UCapsuleComponent>();
	if (IsValid(MyCapsuleComponent))
	{
		FScriptDelegate CapsuleDelegate;
		CapsuleDelegate.BindUFunction(this, FName(TEXT("OnComponentHit")));
		MyCapsuleComponent->OnComponentHit.AddUnique(CapsuleDelegate);
	}

	Niagara = GetComponentByClass<UNiagaraComponent>();

}

void AAwesomeCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentSlindingSpeed != 0.f)
	{
		FVector Direction = CachedSplineComponent->GetDirectionAtDistanceAlongSpline(CurrentSlidingDistance, ESplineCoordinateSpace::World);

		// Update speed depending on slop
		float Slope = Direction.Dot(FVector(0, 0, -SlideDirection) * 100);
		FRichCurve* SlidingRichCurve = SlindingCurve.GetRichCurve();
		if (SlidingRichCurve != nullptr)
		{
			CurrentSlindingSpeed = SlidingRichCurve->Eval(Slope);
		}

		if (CurrentSlindingSpeed >= 2000.f && !bAlreadyPlayedShake)
		{
			bAlreadyPlayedShake = true;

			APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
			if (IsValid(CameraManager))
			{
				CameraManager->StartCameraShake(CameraShake.LoadSynchronous());
			}
		}
		else if (CurrentSlindingSpeed < 2000.f)
		{
			bAlreadyPlayedShake = false;
		}

		// Show speed particules depending on current speed
		FRichCurve* ParticuleRichCurve = ParticuleCurve.GetRichCurve();
		if (IsValid(Niagara) && ParticuleRichCurve != nullptr)
		{
			ParticuleRichCurve->Eval(CurrentSlindingSpeed) > 0 ? Niagara->Activate() : Niagara->Deactivate();
		}

		// Update FOV depending on current speed
		FRichCurve* CameraFOVRichCurve = CameraFOVCurve.GetRichCurve();
		if (IsValid(FollowCamera) && CameraFOVRichCurve != nullptr)
		{
			FollowCamera->SetFieldOfView(CameraFOVRichCurve->Eval(CurrentSlindingSpeed));
		}

		// Update Camera lag depending on current speed
		FRichCurve* CameraLagRichCurve = CameraLagCurve.GetRichCurve();
		if (IsValid(CameraBoom) && CameraLagRichCurve != nullptr)
		{
			CameraBoom->CameraLagSpeed = CameraLagRichCurve->Eval(CurrentSlindingSpeed);
		}

		// Determine next location/rotation
		CurrentSlidingDistance += DeltaTime * CurrentSlindingSpeed * SlideDirection;
		if (CurrentSlidingDistance >= 0.f && CurrentSlidingDistance <= CachedSplineComponent->GetSplineLength())
		{
			FVector OrientedDirection = Direction * SlideDirection;
			FRotator NewRotator = OrientedDirection.Rotation();
			SetActorRotation(NewRotator);

			// Mini-hack (I should fix that properly in the future)
			FVector Padding(0.f, 0.f, 110.f);
			Padding = NewRotator.RotateVector(Padding);

			FVector SplineLocation = CachedSplineComponent->GetLocationAtDistanceAlongSpline(CurrentSlidingDistance, ESplineCoordinateSpace::World);
			FVector NextActorLocation = SplineLocation + Padding;
			SetActorLocation(NextActorLocation);
		}
		else
		{
			StopSliding();
		}
	}
}


void AAwesomeCharacter::StartSliding()
{
	// Determine where we want to start slinding on the spline
	FVector ActorLocation = GetActorTransform().GetLocation();
	float InputKey = CachedSplineComponent->FindInputKeyClosestToWorldLocation(ActorLocation);

	// Determine in which direction we should slide
	FVector ActorForward = GetActorForwardVector();
	FVector TangentAtInputKey = CachedSplineComponent->GetTangentAtSplineInputKey(InputKey, ESplineCoordinateSpace::World);
	SlideDirection = ActorForward.Dot(TangentAtInputKey) < 0 ? -1 : 1;

	// Setting default speed and distance
	CurrentSlindingSpeed = 500.f;
	CurrentSlidingDistance = CachedSplineComponent->GetDistanceAlongSplineAtSplineInputKey(InputKey);

	OldCharacterRotation = GetActorRotation();

	if (IsValid(FollowCamera))
	{
		FollowCamera->SetFieldOfView(90);
	}

	if (IsValid(CameraBoom))
	{
		CameraBoom->bEnableCameraLag = true;
	}

	APlayerCameraManager* CameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
	if (IsValid(CameraManager)) 
	{
		CameraManager->StartCameraShake(CameraShake.LoadSynchronous());
	}
}

void AAwesomeCharacter::StopSliding()
{
	CurrentSlidingDistance = 0.f;
	SlideDirection = 0.f;
	CurrentSlindingSpeed = 0.f;
	bAlreadyPlayedShake = false;

	SetActorRotation(OldCharacterRotation);

	UNiagaraComponent* NiagaraComponent = GetComponentByClass<UNiagaraComponent>();
	if (IsValid(NiagaraComponent))
	{
		NiagaraComponent->Deactivate();
	}

	if (IsValid(FollowCamera))
	{
		FollowCamera->SetFieldOfView(90);
	}

	if (IsValid(CameraBoom))
	{
		CameraBoom->bEnableCameraLag = false;
	}
	

}

//////////////////////////////////////////////////////////////////////////
// Collision

void AAwesomeCharacter::OnComponentHit(UPrimitiveComponent* OnComponentHit, UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// We can ignore this information because we already slide
	if (CurrentSlindingSpeed != 0.f)
	{
		return;
	}

	if (!IsValid(OtherActor))
	{
		return;
	}

	AActor* HitActor = Cast<AActor>(OtherActor->GetOuter());
	if (!IsValid(HitActor))
	{
		return;
	}

	USplineComponent* SplineComponent = HitActor->GetComponentByClass<USplineComponent>();
	if (!IsValid(SplineComponent))
	{
		return;
	}
	CachedSplineComponent = SplineComponent;

	StartSliding();
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAwesomeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AAwesomeCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AAwesomeCharacter::Look);

	}
}

void AAwesomeCharacter::Jump()
{
	if (CurrentSlindingSpeed != 0.f)
	{
		StopSliding();
	}

	Super::Jump();
}

void AAwesomeCharacter::Move(const FInputActionValue& Value)
{
	// We cannot move if we are slinding on splines
	if (CurrentSlindingSpeed != 0.f)
	{
		return;
	}

	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AAwesomeCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}




